package com.gitee.hengboy.mybatis.enhance.dsl.result;

import org.apache.ibatis.mapping.MappedStatement;

/**
 * 重载查询结果定义接口
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/9
 * Time：4:50 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public interface ReloadResult {
    /**
     * 重载方法
     *
     * @param statement           MappedStatement对象实例
     */
    void reload(MappedStatement statement);
}
