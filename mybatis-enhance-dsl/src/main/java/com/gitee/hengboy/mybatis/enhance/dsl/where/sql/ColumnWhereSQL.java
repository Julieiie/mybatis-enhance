package com.gitee.hengboy.mybatis.enhance.dsl.where.sql;

import com.gitee.hengboy.mybatis.enhance.dsl.expression.ColumnExpression;
import com.gitee.hengboy.mybatis.enhance.dsl.where.sql.entity.ColumnWhereSQLEntity;
import com.gitee.hengboy.mybatis.enhance.exception.EnhanceFrameworkException;

/**
 * 查询参数SQL生成接口
 * 如：<p>
 * xx = value
 * xx in (value1,value2)
 * xx not in (value1,value2)
 * </p>
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public interface ColumnWhereSQL {
    /**
     * 获取指定列的查询条件
     *
     * @param columnExpression 列表达式
     * @param valueIndex       表达式内值的索引位置
     * @return 查询条件实体
     * @throws EnhanceFrameworkException 框架异常
     */
    ColumnWhereSQLEntity getColumnWhere(ColumnExpression columnExpression, int valueIndex) throws EnhanceFrameworkException;

    /**
     * 设置参数索引
     *
     * @param paramIndex 参数索引
     * @throws EnhanceFrameworkException 框架异常
     */
    void setParamIndex(int paramIndex) throws EnhanceFrameworkException;
}
