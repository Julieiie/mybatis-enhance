package com.gitee.hengboy.mybatis.enhance.dsl.result.support;

import com.gitee.hengboy.mybatis.enhance.common.helper.RandomHelper;
import com.gitee.hengboy.mybatis.enhance.dsl.result.AbstractReloadResult;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * 基本数据类型重载查询数据返回
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/9
 * Time：4:52 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public class BasicReloadResult extends AbstractReloadResult {
    /**
     * 构造函数初始化返回类型
     *
     * @param parameterResultType 请求参数携带的返回类型
     */
    public BasicReloadResult(Class<?> parameterResultType) {
        super(parameterResultType);
    }

    /**
     * 重新处理基本数据类型返回数据
     *
     * @param statement MappedStatement对象实例
     * @see com.gitee.hengboy.mybatis.enhance.dsl.result.ResultReloadUtils
     * BASIC_CLASS_TYPE
     */
    @Override
    public void reload(MappedStatement statement) {
        /*
         * 重设返回集合到MappedStatement
         */
        ResultMap.Builder builder = new ResultMap.Builder(statement.getConfiguration(), statement.getId() + RandomHelper.generateLowerString(5), parameterResultType, new ArrayList(), true);
        ResultMap resultMap = builder.build();
        MetaObject metaObject = SystemMetaObject.forObject(statement);
        metaObject.setValue("resultMaps", Collections.unmodifiableList(Arrays.asList(resultMap)));
    }
}
